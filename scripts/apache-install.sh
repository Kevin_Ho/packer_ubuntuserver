#!/bin/bash
# This is a script to install a Apache Web Server

# Install apache2
sudo apt update
sudo apt install -y apache2

# Adjusting Firewall
sudo ufw allow 'Apache'
