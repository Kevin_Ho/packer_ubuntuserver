# Packer Example - Ubuntu Server 18.04.1 Vagrant Box

**OS:** Ubuntu Server
**Version:** 18.04.1

## Vagrant box
This example builds a Vagrant Box Ubuntu Server 18.04.1 automated with a Swiss-German keyboard layout.

You can modify the JSON and preseed.cfg according to your wishes.

## Requirements

The following softwares must be installed on the locale machine to build the Vagrant box:

- Packer
- Vagrant
- VirtualBox

## Usage
### Build vagrant box
Make cd in the directory, where the lin.json is placed. Then run:

  $ packer build lin.json

